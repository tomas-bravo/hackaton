import torch
import numpy as np
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from transformers_interpret import SequenceClassificationExplainer


model_checkpoint = "PlanTL-GOB-ES/roberta-base-bne"
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)
model = AutoModelForSequenceClassification.from_pretrained(model_checkpoint)

model.load_state_dict(torch.load('./trained_model.pth', map_location=torch.device('cpu')))
model.eval()

def softmax(x):
    e_x = np.exp(x-np.max(x))
    return e_x / e_x.sum(axis=0)

def evaluate_string(text):
    inputs = tokenizer(text, return_tensors="pt", truncation=True, padding=True)
    with torch.no_grad():
        outputs = model(**inputs)
        predicted_class = outputs.logits.tolist()
        fraudProb = softmax(predicted_class[0])
    return fraudProb[0]

cls_explainer = SequenceClassificationExplainer(
    model,
    tokenizer)

def get_fraud(text):
    predicted_label = evaluate_string(text)
    word_attributions = cls_explainer(text)
    return (predicted_label, word_attributions)

#example_text = "Iba en carretera se me cruza un perro, un camión iba al lado. No tuve más que hacer que golpear con el perro. Posteriormente a eso, el vehículo arroja errores y bota líquido."
#predicted_label = evaluate_string(example_text)
#word_attributions = cls_explainer(example_text)
#print(f"Predicted Label: {predicted_label}")
#print(word_attributions)


