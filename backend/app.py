from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import openai
import model

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_headers=['*'],
    allow_methods=['*']
)

openai.api_key = "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"
# openai.api_base = ''


class Body(BaseModel):
    text: str


template = """Existe un modelo de clasificación de Inteligencia Artificial que intenta identificar si un reporte de siniestro por parte de un cliente de una póliza de seguro es fraudulenta o no. El contexto son seguros de automóviles con clientes Chilenos. El modelo está basado en BERT y utiliza Captum para interpretar como cada token afectó la clasificación. El siguiente párrafo corresponde al reporte del cliente que el modelo determinó como no fraudulento:
    Iba en carretera se me cruza un perro, un camión iba al lado. No tuve más que hacer que golpear con el perro. Posteriormente a eso, el vehículo arroja errores y bota líquido.
    El siguiente párrafo corresponde a la lista de pares de tokens y su respectivo peso de -1 a 1, con valores negativos correspondiendo a no fraude y positivos a fraude, existen errores con la codificación de los caracteres de los tokens:
    {}
    Indica una razón o múltiples razones plausibles para justificar el carácter no fraudulento del reporte, si no existen simplemente limítate a decir que no se encuentran explicaciones. La respuesta debe ser concisa y en lenguaje profesional."""


@app.post("/result")
async def root(body: Body):
    predicted_label, word_attribution = model.get_fraud(body.text)
    # completion = openai.ChatCompletion.create(
    #     model="gpt-3.5-turbo",
    #     messages=[
    #         {"role": "user", "content": template.format([z for z in word_attribution])}
    #     ],
    # )
    return {
        "data": {
            # "response": completion.choices[0].message.content,
            "tokens": word_attribution,
            "prediction": predicted_label,
        }
    }
